/**
 * 
 */
package tp.charles.test;

import static org.junit.Assert.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.vividsolutions.jts.geom.Geometry;

import tp.charles.carto.model.City;
import tp.charles.carto.util.HibernateUtil;

/**
 * @author charles
 *
 */
public class CityTest {

	 private static SessionFactory sessionFactory;
	 private static Session session = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void before() {
		sessionFactory = HibernateUtil.getSessionFactory();
		session = sessionFactory.openSession();
	}

	@Test
	public void test() {
		City city = (City) session
				.get(City.class, 26083);
		
		assertEquals("97356", city.getInsee());
		assertEquals("Camopi", city.getName());
		assertEquals("fr:Camopi", city.getWikipedia());
		assertEquals(1000000000.000000000000000, city.getSurface(), 0);
		assertTrue(city.getBorder() instanceof Geometry);
	}
	
	@After
	public void after() {
		session.close();
		sessionFactory.close();
	}
}
