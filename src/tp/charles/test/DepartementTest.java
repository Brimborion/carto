/**
 * 
 */
package tp.charles.test;

import static org.junit.Assert.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import tp.charles.carto.model.Departement;
import tp.charles.carto.util.HibernateUtil;

/**
 * @author charles
 *
 */
public class DepartementTest {

	 private static SessionFactory sessionFactory;
	 private static Session session = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void before() {
		sessionFactory = HibernateUtil.getSessionFactory();
		session = sessionFactory.openSession();
	}

	@Test
	public void test() {
		Departement departement = (Departement) session
				.get(Departement.class, 1);
		
		assertEquals("Ain", departement.getName());
		assertEquals("01", departement.getCode());
	}
	
	@After
	public void after() {
		session.close();
		sessionFactory.close();
	}
}
