package tp.charles.carto.view;

import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import tp.charles.carto.MainApp;

public class RootLayerController {
	private MainApp mainApp;

	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;
	}

	@FXML
	private void handleQuit() {
		System.exit(0);
	}

	@FXML
	private void handleAbout() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Carto");
		alert.setHeaderText("A propos");
		alert.setContentText("Auteur : Charles Guérin-Rouard\n\n"
				+ "Projet réalisé dans le cadre de l'IMIE\n"
				+ "DL : 2014-2015.");

		alert.showAndWait();
	}
}
