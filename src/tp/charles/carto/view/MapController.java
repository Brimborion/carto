package tp.charles.carto.view;


import com.vividsolutions.jts.geom.Coordinate;

import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import tp.charles.carto.model.City;

public class MapController {
	@FXML
	private Label cityName;

	@FXML
	private Label insee;

	@FXML
	private Label surface;

	@FXML
	private Label wikipedia;

	@FXML
	private Canvas mapCanvas;

	private Stage mapStage;
	private City city;

	@FXML
	private void initialize() {

	}

	public void setMapStage(Stage mapStage) {
		this.mapStage = mapStage;
	}

	public void setCity(City city) {
		this.city = city;

		this.cityName.setText(city.getName());
		this.insee.setText(city.getInsee());
		this.surface.setText(Double.toString(city.getSurface()));
		this.wikipedia.setText(city.getWikipedia());

		GraphicsContext gc = this.mapCanvas.getGraphicsContext2D();
		
		int nPoints = city.getScaledBorder().getNumPoints();
		double[] xPoints = new double[nPoints];
		double[] yPoints = new double[nPoints];
		int i = 0;
		for (Coordinate coordinate : city.getBorder().getCoordinates()) {
			xPoints[i] = coordinate.x;
			yPoints[i] = coordinate.y;
			i++;
		}
		
		Coordinate centerBorder = city.getBorder().getEnvelopeInternal().centre();
		Coordinate centerCanvas = new Coordinate(this.mapCanvas.getWidth()/2, this.mapCanvas.getHeight()/2);
		gc.translate((centerCanvas.x - centerBorder.x), (centerCanvas.y - centerBorder.y));
		gc.strokePolygon(xPoints, yPoints,nPoints);

		//      Pour tester
		//		gc.strokePolygon(new double[]{60, 90, 60, 90},
		//                new double[]{210, 210, 240, 240}, 4);
	}

	@FXML
	private void handleCancel() {
		this.mapStage.close();
	}
}
