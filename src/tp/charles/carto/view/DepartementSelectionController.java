package tp.charles.carto.view;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import tp.charles.carto.MainApp;
import tp.charles.carto.model.City;
import tp.charles.carto.model.Departement;
import tp.charles.carto.util.HibernateUtil;

public class DepartementSelectionController {
	@FXML
	private ListView<Departement> departementList;
	
	@FXML
	private ListView<City> cityList;
	
	private ObservableList<City> cityObservableList =
			FXCollections.observableArrayList();

	private ObservableList<Departement> departementObservableList =
			FXCollections.observableArrayList();

	private MainApp mainApp;

	/**
	 * Constructeur vide.
	 */
	public DepartementSelectionController() {
	}

	/**
	 * Méthode d'initialisation appelée au chargement de DepartementSlection.fxml.
	 */
	@FXML
	private void initialize() {
		// Affichage des départements.
		departementList.setCellFactory(cellData -> {
			return new ListCell<Departement>(){
					@Override
					protected void updateItem(Departement item, boolean empty) { 
				        super.updateItem(item, empty); 
				        setText(null); 
				        if (!empty && item != null) { 
				            final String text = item.getCode() + " " + item.getName(); 
				            setText(text); 
				        } 
		    		}
			};
		});
		
		// Ecouteur pour la sélection du département et affichage des communes si sélection.
		departementList.getSelectionModel().selectedItemProperty().addListener(
				(observable, oldValue, newValue) -> this.showCities(newValue));
	}

	/**
	 * Appeler par mainApp pour faire référence à elle-même.
	 * @param mainApp
	 */
	public void setMainApp(MainApp mainApp) {
		this.mainApp = mainApp;

		this.departementList.setItems(getDepartementObservableList());
	}
	
	/**
	 * Affichage de la liste des communes d'un département donné.
	 * @param departement
	 */
	public void showCities(Departement departement) {
		this.cityList.setItems(getCityObservableList(departement));
		cityList.setCellFactory(cellData -> {
			return new ListCell<City>(){
				@Override
				protected void updateItem(City item, boolean empty) { 
			        super.updateItem(item, empty); 
			        setText(null); 
			        if (!empty && item != null) { 
			            final String text = item.getName(); 
			            setText(text); 
			        } 
	    		}
			};
		});
	}

	/**
	 * Retourne la liste des départements avec écoute pour JavaFX.
	 * @return Liste des départements.
	 */
	public ObservableList<Departement> getDepartementObservableList() {
		if(!this.departementObservableList.isEmpty()) {
			this.departementObservableList.clear();
		}
		this.departementObservableList = FXCollections.observableList(
				(List<Departement>) getDepartementList());

		return this.departementObservableList;
	}
	
	/**
	 * Retourne la liste des villes d'un département avec écoute pour JavaFX.
	 * @return Liste des départements.
	 */
	public ObservableList<City> getCityObservableList(Departement departement) {
		if(!this.cityObservableList.isEmpty()) {
			this.cityObservableList.clear();
		}
		this.cityObservableList = FXCollections.observableList(
				(List<City>) getCityList(departement));
		
		return this.cityObservableList;
	}

	/**
	 * Retourne la liste des départements.
	 * @return Liste des départements.
	 */
	public List<Departement> getDepartementList() {
		List<Departement> departementList = new ArrayList<>();

		Session session = HibernateUtil.getSessionFactory().openSession();
		departementList = session.createCriteria(Departement.class)
				.addOrder(Order.asc("code"))
				.list();
		session.close();

		return departementList;
	}
	
	/**
	 * Retourne la liste des villes d'un département.
	 * @param departement
	 * @return
	 */
	public List<City> getCityList(Departement departement) {
		List<City> cityList = new ArrayList<>();
		List<String> resultList = new ArrayList<>();

		Session session = HibernateUtil.getSessionFactory().openSession();
		cityList = session.createCriteria(City.class)
				.add(Restrictions.like("insee", departement.getCode() + "%"))
				.addOrder(Order.asc("name"))
				.list();
		session.close();

		return cityList;
	}
	
	@FXML
	private void handleSelectedCity() {
		City selectedCity = cityList.getSelectionModel().getSelectedItem();
		mainApp.showMap(selectedCity);
	}
}
