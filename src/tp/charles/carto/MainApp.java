package tp.charles.carto;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import tp.charles.carto.model.City;
import tp.charles.carto.view.DepartementSelectionController;
import tp.charles.carto.view.MapController;

public class MainApp extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;

	@Override
	public void start(Stage primaryStage) {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("CartoApp");

		initRootLayout();

		showDepartementSelection();
	}

	/**
	 * Initialise le layout principal.
	 */
	public void initRootLayout() {
		try {
			// Charge le layout de base.
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/RootLayout.fxml"));
			rootLayout = (BorderPane) loader.load();

			// Affiche la scene contenant le layout de base.
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Affiche la vue de sélection dans le layout principal.
	 */
	public void showDepartementSelection() {
		try {
			// Chargement de la vue de sélection
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/DepartementSelection.fxml"));
			AnchorPane departementSelection = (AnchorPane) loader.load();

			// Positionne la vu de sélection au centre du layout de base
			rootLayout.setCenter(departementSelection);
			
			// Donne accès à MainApp pour le controlleur.
			DepartementSelectionController controller = loader.getController();
			controller.setMainApp(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void showMap(City city) {
		try {
			// Chargement de la vue de sélection
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource("view/Map.fxml"));
			AnchorPane map = (AnchorPane) loader.load();
			
			// Création de la fenêtre.
			Stage mapStage = new Stage();
			mapStage.setTitle(city.getName() + " : détails");
			mapStage.initModality(Modality.WINDOW_MODAL);
			mapStage.initOwner(this.primaryStage);
			Scene scene = new Scene(map);
			mapStage.setScene(scene);
			
			// Injection de la ville dans le contrôleur.
			MapController controller = loader.getController();
			controller.setMapStage(mapStage);
			controller.setCity(city);
			
			// Affichage de la fenêtre + attente de fermeture
			mapStage.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Retourne le stage principal.
	 * @return
	 */
	public Stage getPrimaryStage() {
		return this.primaryStage;
	}

	/**
	 * Méthode principale.
	 * @param args
	 */
	public static void main(String[] args) {
		launch(args);
	}
}
