package tp.charles.carto.model;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Entité représentant les départements.
 * @author charles
 *
 */
public class Departement {
	private int id;
	private final StringProperty name;
	private final StringProperty code;

	/**
	 * Constructeur par défaut.
	 */
	public Departement() {
		this(null, null);
	}
	/**
	 * Constructeur principal.
	 * @param name Nom du département.
	 * @param code Code du département.
	 */
	public Departement(String name, String code) {
		this.name = new SimpleStringProperty(name);
		this.code = new SimpleStringProperty(code);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	public StringProperty nameProperty() {
		return this.name;
	}

	public java.lang.String getName() {
		return this.nameProperty().get();
	}

	public void setName(final java.lang.String name) {
		this.nameProperty().set(name);
	}

	public StringProperty codeProperty() {
		return this.code;
	}

	public java.lang.String getCode() {
		return this.codeProperty().get();
	}

	public void setCode(final java.lang.String code) {
		this.codeProperty().set(code);
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Departement [name=" + name + ", code=" + code + "]";
	}
}
