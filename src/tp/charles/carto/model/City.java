/**
 * 
 */
package tp.charles.carto.model;

import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.util.AffineTransformation;

import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Entité pour les communes.
 * @author charles
 *
 */
public class City {
	private int id;
	private final StringProperty name;
	private final StringProperty insee;
	private final StringProperty wikipedia;
	private final DoubleProperty surface;
	private Geometry border;
	/**
	 * Constructeur vide.
	 */
	public City() {
		this(null, null, null, 0.0);
	}
	/**
	 * Constructeur principal.
	 * 
	 * @param name		Nom de la commune
	 * @param insee		Code INSEE de la commune
	 * @param wikipedia Page wikipedia
	 * @param surface	Surface de la commune en m²
	 */
	public City(String name, String insee, String wikipedia, double surface) {
		this.name = new SimpleStringProperty(name);
		this.insee = new SimpleStringProperty(insee);
		this.wikipedia = new SimpleStringProperty(wikipedia);
		this.surface = new SimpleDoubleProperty(surface);
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}
	public StringProperty nameProperty() {
		return this.name;
	}

	public java.lang.String getName() {
		return this.nameProperty().get();
	}

	public void setName(final java.lang.String name) {
		this.nameProperty().set(name);
	}

	public StringProperty inseeProperty() {
		return this.insee;
	}

	public java.lang.String getInsee() {
		return this.inseeProperty().get();
	}

	public void setInsee(final java.lang.String insee) {
		this.inseeProperty().set(insee);
	}

	public StringProperty wikipediaProperty() {
		return this.wikipedia;
	}

	public java.lang.String getWikipedia() {
		return this.wikipediaProperty().get();
	}

	public void setWikipedia(final java.lang.String wikipedia) {
		this.wikipediaProperty().set(wikipedia);
	}

	public DoubleProperty surfaceProperty() {
		return this.surface;
	}

	public double getSurface() {
		return this.surfaceProperty().get();
	}

	public void setSurface(final double surface) {
		this.surfaceProperty().set(surface);
	}
	/**
	 * @return the border
	 */
	public Geometry getBorder() {
		return border;
	}
	/**
	 * @param border the border to set
	 */
	public void setBorder(Geometry border) {
		this.border = border;
	}

	/**
	 * Retourne le contours de la ville mise à l'échelle.
	 * @return Contours de la ville à l'échelle.
	 */
	public Geometry getScaledBorder() {
		Geometry scaledBorder = (Geometry) this.border.clone();
		
		// Ne fonctionne pas.
		scaledBorder.apply(AffineTransformation.scaleInstance(10, 10));
		
		return scaledBorder;
	}

	@Override
	public String toString() {
		return "City [id=" + id + ", name=" + name + ", insee=" + insee + ", wikipedia=" + wikipedia + ", surface="
				+ surface + ", border=" + border + "]";
	}
}
